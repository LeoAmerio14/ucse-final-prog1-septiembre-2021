﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Piedra : Menu
    {
        public override int CalcularDemora(PorcionesPizza porciones, TipoPizza tipo)
        {
            if (TipoPizza.Piedra == tipo)
            {
                switch (porciones)
                {
                    case PorcionesPizza.Ocho:
                        Demora = 10;
                        break;
                    case PorcionesPizza.Diez:
                        Demora = 15;
                        break;
                    case PorcionesPizza.Doce:
                        Demora = 20;
                        break;

                }            
            }
            return Demora;
        }
    }
}
