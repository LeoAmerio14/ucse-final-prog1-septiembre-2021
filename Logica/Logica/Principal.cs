﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        public List<Pedido> Pedidos = new List<Pedido>();

        public void GenerarNuevoPedido(string cliente, Menu pedidoCliente)
        {
            Pedido pedido = new Pedido();
            pedido.Cliente = cliente;
            pedido.EstadoPizza = Pedido.Estado.Pendiente;
            pedido.PedidoDeMenu.Porciones = pedidoCliente.Porciones;
            pedido.PedidoDeMenu.Tipo = pedidoCliente.Tipo;
            pedido.DemoraEsimada = pedidoCliente.CalcularDemora(pedido.PedidoDeMenu.Porciones, pedido.PedidoDeMenu.Tipo);
            pedido.FechaPedido = DateTime.Now;
            Pedidos.Add(pedido);
            
        }

        public void Actualiacion(string nombreCliente)
        {
            Pedido pedido = Pedidos.Find(x => x.Cliente == nombreCliente && x.EstadoPizza == Pedido.Estado.Pendiente);
            if(pedido != null)
            {
                pedido.EstadoPizza = Pedido.Estado.Entregado;
                pedido.FechaEntrega = DateTime.Now;
            }
        }

        public double CalcularIngresos(DateTime fecha1, DateTime fecha2)
        {
            double MontoTotal = 0;
            foreach (var item in Pedidos)
            {
                if (item.FechaEntrega > fecha1 && item.FechaEntrega < fecha2)
                {
                    MontoTotal += item.PedidoDeMenu.Precio;
                }
            }
            return MontoTotal;
        }

        public PedidosXTiempo CalcularMonto()
        {
            PedidosXTiempo pedidos = new PedidosXTiempo();
            foreach (var item in Pedidos)
            {
                if(item.FechaEntrega > pedidos.Fecha1 && item.FechaEntrega < pedidos.Fecha2)
                {
                    pedidos.MontoTotal += item.PedidoDeMenu.Precio;
                    pedidos.Cantidad += 1;
                }
            }
            return pedidos;
        }

        public double CalcularDemora()
        {
            double Acu = 0;
            int Contador = 0;
            foreach (var item in Pedidos)
            {
                if(item.EstadoPizza == Pedido.Estado.Entregado)
                {
                    Acu += item.PedidoDeMenu.CalcularDemora(item.PedidoDeMenu.Porciones, item.PedidoDeMenu.Tipo);
                    Contador++;
                }
            }
            return (Acu / Contador);

            
        }
    }
}
