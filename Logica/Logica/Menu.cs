﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    ///Problema de modelado, no usa las clases para polimorfismo sino que basa el desarrollo en el enum.

    public abstract class Menu
    {
        //NO ES NECESARIO, LO TENES EN LAS SUBCLASES
        public enum TipoPizza { Piedra, Parrilla, Molde }

        public string Nombre { get; set; }
        public List<string> Ingredientes { get; set; }
        public double Precio { get; set; }
        public enum PorcionesPizza { Ocho, Diez, Doce }
        public PorcionesPizza Porciones { get; set; }
        //NO ES NECESARIO, LO TENES EN LAS SUBCLASES
        public TipoPizza Tipo { get; set; }
        public int Demora { get; set; }

        //El tipo no es necesario porq se implementa en las subclases, ahi ya sabes de que tipo es.
        public abstract int CalcularDemora(PorcionesPizza porciones, TipoPizza tipo);
    }
}
