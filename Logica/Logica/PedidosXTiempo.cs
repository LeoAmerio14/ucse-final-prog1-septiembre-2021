﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class PedidosXTiempo
    {
        public DateTime Fecha1 { get; set; }
        public DateTime Fecha2 { get; set; }
        public double MontoTotal { get; set; }
        public int Cantidad { get; set; }
    }
}
