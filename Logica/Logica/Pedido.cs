﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Pedido
    {
        public string Cliente { get; set; }
        public Menu PedidoDeMenu { get; set; }
        public DateTime FechaPedido { get; set; }
        public DateTime FechaEntrega { get; set; }
        public double DemoraEsimada { get; set; }
        public enum Estado { Pendiente, Entregado }
        public Estado EstadoPizza { get; set; }
        public double Total { get; set; }
        public DateTime DiferenciaHoraria { get; set; }



    }
}
